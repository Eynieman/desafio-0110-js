/* 1.- Escribe un programa de una sola línea que haga que aparezca en la pantalla un alert que diga “un mensaje”.

alert ("Un mensaje");

// 2.- Escribe un programa de una sola línea que escriba en la pantalla un texto que diga «Hello World» (document.write).

document.write('Hello World','</br>');

// 3.- Escribe un programa de una sola línea que escriba en la pantalla el resultado de sumar 3 + 5.

document.write(parseInt(3)+parseInt(5),'</br>')

// 4.- Escribe un programa de dos líneas que pida el nombre del usuario con un prompt y escriba un texto que diga «Hola nombreUsuario»

var nombreUsuario = prompt('Ingrese su nombre');
document.write('Hola',' ', nombreUsuario,'</br>');

// 5.- Escribe un programa de tres líneas que pida un número, pida otro número y escriba el resultado de sumar estos dos números.

var n1 = prompt('Ingrese el valor de numero 1');
var n2 = prompt('Ingrese el valor de numero 2');
document.write(parseInt(n1) + parseInt(n2));

// 6.- Escribe un programa que pida dos números y escriba en la pantalla cual es el mayor.

var n1 = prompt('Ingrese el valor de numero 1');
var n2 = prompt('Ingrese el valor de numero 2');
if (n1 > n2)
{
    document.write(parseInt(n1));
}
else {
    document.write(parseInt(n2));
}

// 7.- Escribe un programa que pida 3 números y escriba en la pantalla el mayor de los tres.

var n1 = prompt('Ingrese el valor de numero 1');
var n2 = prompt('Ingrese el valor de numero 2');
var n3 = prompt('Ingrese el valor de numero 3');
if (n1 > n2 && n1 > n3)
{
    document.write(parseInt(n1));
}
else if (n2 > n3)
{
    document.write(parseInt(n2));
}
else
{
    document.write(parseInt(n3));
}

// 8.- Escribe un programa que pida un número y diga si es divisible por 2.

var n1 = prompt('Ingrese el valor del numero');
if (n1 % 2 == 0)
{
    document.write('El número ingresado es divisible en 2.');
}
else
{
    document.write('El número que ingresó no es divisible en 2.');
}

// 9.- Escribe un programa que pida una frase y escriba las vocales que aparecen.
var text = prompt('Escriba su frase');
var nText = text.length;
var i;
for (i = 0; i < nText; i++)
{
    if (text.substr(i, 1) === "a" || text.substr(i, 1) === "e" || text.substr(i, 1) === "i" || text.substr(i, 1) === "o" || text.substr(i, 1) === "u")
    {
        document.write(text.substr(i, 1));
    }
}

// 10.- Escribe un programa que pida un número y nos diga si es divisible por 2, 3, 5 o 7 (sólo hay que comprobar si lo es por uno de los cuatro).
var n1 = prompt('Ingrese el valor del numero');
if (n1 % 2 == 0 || n1 % 3 == 0 || n1 % 5 == 0 || n1 % 7 == 0)
{
    document.write('El número ingresado es divisible por 2, 3, 5 o 7');
}
else
{
    document.write('El número que ingresó no es divisible por 2, 3, 5 o 7');
}

// 11.- Añadir al ejercicio anterior que nos diga por cuál de los cuatro es divisible (hay que decir todos por los que es divisible)
var n1 = prompt('Ingrese el valor del numero');
if (n1 % 2 == 0 || n1 % 3 == 0 || n1 % 5 == 0 || n1 % 7 == 0)
{
    if (n1 % 2  == 0)
    {
        document.write('El número ingresado es divisible por 2.','</br>');
    }
    if (n1 % 3  == 0)
    {
        document.write('El número ingresado es divisible por 3.','</br>');
    }
    if (n1 % 5  == 0)
    {
        document.write('El número ingresado es divisible por 5.','</br>');
    }
    if (n1 % 7  == 0)
    {
        document.write('El número ingresado es divisible por 7.','</br>');
    }
}
else
{
    document.write('El número que ingresó no es divisible por 2, 3, 5 o 7');
}
*/